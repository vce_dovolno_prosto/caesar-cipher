using NUnit.Framework;
using Course_work_22_04.Models;
using System.IO;


namespace Tests
{
    [TestFixture, Parallelizable]
    class TextServiceTest
    {
        [TestCase("�������� � ������������� ������� �������� ������. �� ��� ���������� ������� ����������, ����� ����� ����������� � ��������� ��������� ��������, ��������, �� ���� ������� � �������.")]
        [TestCase("���� ������ �������� ����������� ������������ �������� � ������ ��������, ������� ����������� �� ��� ���, �������� � ��������� ������� ��������� ����� ����������������� ��������.")]
        public void TxtWritingAndReading(string expectedResult)
        {
            byte[] bytes = TextService.GetTxtFileWithText(expectedResult);
            using (var ms = new MemoryStream(bytes))
            {
                string result = TextService.GetTextFromTxt(ms);
                Assert.AreEqual(expectedResult, result);
            }
        }
    }

}
