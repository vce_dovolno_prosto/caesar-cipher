using NUnit.Framework;
using Course_work_22_04.Models;


namespace Tests
{
    public class CaesarTests
    {
        [TestCase("�", 1, "�")]
        [TestCase("�", -1, "�")]
        [TestCase("�", 0, "�")]
        [TestCase("�", 33, "�")]
        [TestCase("�", -33, "�")]
        [TestCase("�", 34, "�")]
        [TestCase("�", -34, "�")]
        [TestCase(":", 99, ":")]
        [TestCase(";", 99, ";")]
        [TestCase("3", 99, "3")]
        [TestCase("�", 1, "�")]
        [TestCase("�", -330, "�")]

        public void ShiftTest(string input, int shift, string expectedResult)
        {
            string result = Caesar.Shift(input, shift);
            Assert.AreEqual(expectedResult, actual: result);
        }


        private const string teacherExample = "���������, �� ������� ������� �����!!! " +
            "� �������� ������, ��� ��� ������������ ���� ������ �� ����� ������, �������� �������� ������� � ���, ����� �������� ���� �������� �" +
            " ����������� � ����������� ����������� � ���� �����.������ ���� �������� �� �����, ������� ��������� �� ����������� �����, ��������� " +
            "��� ������� ������ � ������������ ���� ������! ������, ��� ���� ���������� ������� � ���������� ��� � ��������� ������, �� ������ ���" +
            " ��� ��� ��������� ��������, � � ������ ����� ���������! �� ���� �������� FirstLineSoftware � ������������ ����, � �� ��������� ���� �" +
            " ����������� ���������� ����� ������ �# ��� ����������! �� ����� �������� ������� � ���������� ���������� � ��� �� � ���������������� � " +
            "�������������� ����� ���������� .Net, ���� �������� � ���������� ����!";
        [TestCase(teacherExample, 2)]
        public void KeySearchTest(string input, int expectedResult)
        {
            int[] keys = Caesar.GetBestKey(input);
            Assert.AreEqual(expectedResult, keys[0]);
        }
    }
}