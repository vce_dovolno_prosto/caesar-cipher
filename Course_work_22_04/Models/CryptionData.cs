﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Course_work_22_04.Models
{
    public class CryptionData
    {
        public static CryptionData None = new CryptionData();

        public int Shift { get; set; }
        public File File { get; set; }
    }
}