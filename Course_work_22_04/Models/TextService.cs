﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;
using System.IO;
using NPOI.XWPF.UserModel;

namespace Course_work_22_04.Models
{
    public static class TextService
    {
        private static string GetTempDocxPath(Controller controll)
        {
            return controll.Server.MapPath("~/Models/" + DateTime.Now.ToFileTime());
        }

        public static string GetTextFromTxt(Stream stream)
        {
            return new StreamReader(stream, Encoding.Default).ReadToEnd();
        }

        public static string GetTextFromDocx(Stream stream, Controller controll)
        {
            StringBuilder wholeDocument = new StringBuilder();
            XWPFDocument document = new XWPFDocument(stream);
            foreach (var item in document.Paragraphs)
                wholeDocument.AppendLine(item.Text);
            return wholeDocument.ToString();
        }

        public static byte[] GetTxtFileWithText(string text)
        {
            return Encoding.Default.GetBytes(text);
        }

        public static byte[] GetDocFileWithText(string text)
        {
            XWPFDocument document = new XWPFDocument();
            foreach (var item in text.Split('\n'))
            {
                var paragraph = document.CreateParagraph();
                var run = paragraph.CreateRun();
                run.SetText(item);
            }


            MemoryStream stream = new MemoryStream();
            document.Write(stream);

            return stream.ToArray();
        }


    }
}