﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Course_work_22_04.Models
{
    public static class Caesar
    {

        private class Key
        {
            public int Shift;
            public float Deflection;
        }

        public const string alphabetLower = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        private const string alphabetUpper = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

        private static readonly string[] singleLetterWord = { "а", "б", "в", "ж", "и", "к", "о", "с", "у", "э", "ю", "я" };

        private static readonly float[] letterFrecuency = { 0.0801f, 0.0159f, 0.0454f, 0.0170f, 0.0298f, 0.0845f, 0.0004f, 0.0094f, 0.0165f, 0.0735f,
                0.0121f, 0.0349f, 0.0440f, 0.0321f, 0.0670f, 0.1097f, 0.0281f, 0.0473f, 0.0547f, 0.0626f, 0.0262f, 0.0026f, 0.0097f, 0.0048f, 0.0144f,
                0.0073f, 0.0036f, 0.0004f, 0.0190f, 0.0174f, 0.0032f, 0.0064f, 0.0201f };


        public static string Shift(string text, int shift) // Функция сдвига 
        {
            shift = shift < 0 ? 33 + shift % 33 : shift;

            string shiftText = "";
            int index;
            foreach (var c in text)
            {
                if ((index = alphabetLower.IndexOf(c)) != -1)
                {
                    shiftText += alphabetLower[(index + shift) % alphabetLower.Length];
                }
                else if ((index = alphabetUpper.IndexOf(c)) != -1)
                {
                    shiftText += alphabetUpper[(index + shift) % alphabetUpper.Length];
                }
                else
                {
                    shiftText += c;
                }
            }
            return shiftText;
        }

        public static int[] GetBestKey(string text) // функция поиска наилучшего ключа, основанная на частотном анализе ( out приоритет ключей )              
        {
            text = text.ToLower();
            int index;
            int russianLetterCounter = 0;
            int[] russianLetterCount = new int[alphabetLower.Length];
            string russianText = "";
            foreach (var c in text)
            {
                if ((index = alphabetLower.IndexOf(c)) != -1)
                {
                    russianLetterCount[index]++;
                    russianLetterCounter++;
                    russianText += c;
                }
                else if (c == ' ')
                    russianText += ' ';
            }

            float currentFrecuency;
            int keysCounter = alphabetLower.Length;
            var keys = new List<Key>(keysCounter);
            for (int i = 0; i < keysCounter; i++)
            {
                var key = new Key() { Shift = i, Deflection = 0 };
                for (int j = 0; j < alphabetLower.Length; j++)
                {
                    index = (j + i) % russianLetterCount.Length;
                    currentFrecuency = russianLetterCount[index] * 1f / russianLetterCounter;
                    key.Deflection += Math.Abs(currentFrecuency - letterFrecuency[j]); // Ищем среднеквадратическое отклонение
                }
                keys.Add(key);
            }

            string shiftRussianText;  // Улучшим результаты, увеличивая отклонение ключам с невозможными словами 
            string[] words;
            foreach (var key in keys)
            {
                shiftRussianText = Shift(russianText, -key.Shift);
                words = shiftRussianText.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var word in words)
                {
                    if (word.StartsWith("ъ") || word.StartsWith("ь") || word.StartsWith("ы"))
                        key.Deflection *= 100;
                    if (word.Length == 1 && !singleLetterWord.Contains(word))
                        key.Deflection *= 100;
                }
            }
            // Сортируем ключи
            return keys.OrderBy(key => key.Deflection).Select(x => x.Shift).ToArray(); // Вывод массива приоретета ключей 
        }


    }

}