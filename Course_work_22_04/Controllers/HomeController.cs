﻿using Course_work_22_04.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Course_work_22_04.Controllers
{
    public class HomeController : Controller
    {
        public FileContext db = new FileContext();
        public List<ID_Key> ListId = new List<ID_Key>();

        
        private string ButtonTextToAction(string action)
        {
            switch (action)
            {
                case "Зашифровать": return "Encrypt";
                case "Расшифровать": return "Decryption";
                case "Расшифровать без ключа": return "KeySearch";
            }
            return "";
        }
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file1, string text1, HttpPostedFileBase file2, string text2, HttpPostedFileBase file3, string text3, string cryptionKey, string decryptionKey, string action, HttpPostedFileBase file, string text)
        {
            action = ButtonTextToAction(action);
            if (action == "Encrypt")
            { file = file1; text = text1; }

            if (action == "Decryption")
            { file = file2; text = text2; }

            if (action == "KeySearch")
            { file = file3; text = text3; }

            if (file == null && text.Length < 1)
                return RedirectToAction("Index", "Home");
            
            string key = action == "Encrypt" ? cryptionKey : decryptionKey;


            string fileName;
            int id;

            if (file != null)
            {
                fileName = file.FileName;
                if (file.FileName.EndsWith(".txt"))
                {
                    text = TextService.GetTextFromTxt(file.InputStream);
                }
                else
                    text = TextService.GetTextFromDocx(file.InputStream, this);
                
                id = db.Add(file.FileName, text);
            }
            else
                id = db.Add(text);

            ID_Key id_Key = new ID_Key();
            id_Key.Id = id;
            id_Key.Key = key;
            ListId.Add(id_Key);
            TempData["ListId"] = ListId;
            return RedirectToAction(action, "WorkResult");
        }




        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Contact()
        {        
            return View();
        }

        public ActionResult Work()
        {
            return View();
        }

    }
}