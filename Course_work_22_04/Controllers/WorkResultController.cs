﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Course_work_22_04.Models;




namespace Course_work_22_04.Controllers
{
    public class WorkResultController : Controller
    {

        // GET: Work


        FileContext db = new FileContext();

        private CryptionData GetCryptionData(List<ID_Key> ListId)
        {
            var data = new CryptionData();          
            
            if (ListId == null)
                return CryptionData.None;
            int fileId;
            try
            {
                fileId = ListId[0].Id;
            }
            catch
            {
                return CryptionData.None;
            }
            var file = db.GetById(fileId);
            if (file == null)
                return CryptionData.None;

            data.File = file;
            data.Shift = Convert.ToInt32(ListId[0].Key);
            return data;
        }

        [HttpPost]
        public ActionResult Download(string shift)
        {
            List<ID_Key> ListId = (List<ID_Key>)TempData["ListId"];
            var data = GetCryptionData(ListId);
            if (data == CryptionData.None)
                return RedirectToAction("Index", "Home");

            int iShift = int.Parse(shift);
            byte[] bytes;
            string fileType;
            string text;
            if (data.File.Name.EndsWith(".txt"))
            {
                fileType = "text/plain";
                text = Caesar.Shift(data.File.Text, iShift);
                bytes = TextService.GetTxtFileWithText(text);
            }
            else
            {
                fileType = "application/msword";
                text = Caesar.Shift(data.File.Text, iShift);
                bytes = TextService.GetDocFileWithText(text);
            }


            return File(bytes, fileType, data.File.Name);
        }
        public ActionResult Encrypt()
        {
            List<ID_Key> ListId = (List<ID_Key>)TempData["ListId"];
            var data = GetCryptionData(ListId);
            if (data == CryptionData.None)
                return RedirectToAction("Index", "Home");

            ViewBag.Key = data.Shift;
            ViewBag.Text = Caesar.Shift(data.File.Text, data.Shift); 
            TempData["ListId"] = ListId;
            return View();
        }

        public ActionResult Decryption()
        {
            List<ID_Key> ListId = (List<ID_Key>)TempData["ListId"];
            var data = GetCryptionData(ListId);
            if (data == CryptionData.None)
                return RedirectToAction("Index", "Home");

            ViewBag.Key = data.Shift;
            ViewBag.Text = Caesar.Shift(data.File.Text, -data.Shift); ;
            TempData["ListId"] = ListId;
            return View();
        }

        public ActionResult KeySearch()
        {
            List<ID_Key> ListId = (List<ID_Key>)TempData["ListId"];
            var data = GetCryptionData(ListId);
            if (data == CryptionData.None)
                return RedirectToAction("Index", "Home");

            ViewBag.Keys = Caesar.GetBestKey(data.File.Text);
            ViewBag.Text = data.File.Text;
            TempData["ListId"] = ListId;
            return View();
        }


    }
}